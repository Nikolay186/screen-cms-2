import { Injectable, CanActivate, ExecutionContext, NotFoundException, ForbiddenException } from "@nestjs/common";
import { getRepository } from "typeorm";
import { Screen } from "../screen.entity";
import { User } from "src/user/user.entity";

@Injectable()
export class ScreenOwnerGuard implements CanActivate {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const screenRepository = getRepository(Screen);
        const userRepository = getRepository(User);
        const screen_id = request.params.id;

        const modScreen = await screenRepository.findOne({
            where: { id: screen_id }
        });
        if (!modScreen) {
            throw new NotFoundException('Screen specified does not exists');
        }
        const owner = await userRepository.findOne({
            where: { id: modScreen.owner_id }
        });

        const user = request.user;
        if (user.id == owner.id) {
            return true;
        } else {
            throw new ForbiddenException('You are not the owner of specified screen');
        }
    }
}