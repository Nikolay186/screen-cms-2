import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { auth } from 'express-openid-connect';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  require('dotenv').config({path: './.env'});

  const sw_cfg = new DocumentBuilder()
    .setTitle('Screen CMS')
    .setDescription('Swagger for the Screen CMS app')
    .setVersion('1.0')
    .build();
  const doc = SwaggerModule.createDocument(app, sw_cfg);
  SwaggerModule.setup('showapi', app, doc);

  const auth_cfg = {
    authRequired: false,
    auth0Logout: true,
    secret: process.env.AUTH0_SECRET,
    baseURL: 'http://localhost:3000',
    clientID: process.env.AUTH0_CLIENT_ID,
    issuerBaseURL: process.env.AUTH0_ISSUER_URL,
  };

  app.use(auth(auth_cfg));

  app.enableCors();
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}
bootstrap();
