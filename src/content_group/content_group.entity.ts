// import { Content } from "src/content/content.entity";
import { User } from "src/user/user.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";

@Entity('Content_group')
export class ContentGroup {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column()
    owner_id: string;

    @OneToMany(() => Content, content => content.group_id)
    content: Content[];

    @ManyToOne(() => User, user => user.groups, {
        cascade: true,
        onDelete: 'CASCADE',
    })
    owner: User;
}