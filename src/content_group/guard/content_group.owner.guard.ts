import { Injectable, CanActivate, ExecutionContext, NotFoundException, ForbiddenException } from "@nestjs/common";
import { User } from "src/user/user.entity";
import { getRepository } from "typeorm";
import { ContentGroup } from "../content_group.entity";

@Injectable()
export class GroupOwnerGuard implements CanActivate {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const groupReposirory = getRepository(ContentGroup);
        const userRepository = getRepository(User);

        const groupId = request.params.id;
        const modGroup = await groupReposirory.findOne({
            where: { id: groupId }
        });
        if (!modGroup) {
            throw new NotFoundException('Specified group does not exists');
        }
        const owner = await userRepository.findOne({
            where: { id: modGroup.owner_id }
        })

        const user = request.user;
        if (user.id == owner.id) {
            return true;
        } else {
            throw new ForbiddenException('You are not the owner of specified group');
        }
    }
}