import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Repository } from "typeorm";
import { ContentGroup } from "./content_group.entity";

@Injectable()
export class ContentGroupCrudService extends TypeOrmCrudService<ContentGroup> {
    constructor(@InjectRepository(ContentGroup) groupRepository: Repository<ContentGroup>)
    {
        super(groupRepository);
    }
}