import { Controller, Injectable, UseGuards } from "@nestjs/common";
import { AuthGuard } from "@nestjs/passport";
import { ApiAcceptedResponse, ApiUnauthorizedResponse, ApiNotFoundResponse, ApiTags, ApiBearerAuth } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { GroupOwnerGuard } from "./guard/content_group.owner.guard";
import { ContentGroup } from "./content_group.entity";
import { ContentGroupCrudService } from "./content_group.service";

@Crud({
    model: {
        type: ContentGroup,
    },
    routes: {
        only: ['createOneBase', 'getOneBase', 'getManyBase', 'updateOneBase', 'deleteOneBase'],
        createOneBase: {
            decorators: [
                UseGuards(AuthGuard('jwt')),
                ApiAcceptedResponse({ description: 'Creates group' }),
                ApiUnauthorizedResponse({ description: 'Not authenticated' }),
            ],
        },
        getOneBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns specified group' }),
                ApiNotFoundResponse({ description: 'Not found' })
            ],
        },
        getManyBase: {
            decorators: [
                ApiAcceptedResponse({ description: 'Returns all groups' })
            ],
        },
        updateOneBase: {
            decorators: [
                UseGuards(GroupOwnerGuard),
                UseGuards(AuthGuard('jwt')),
                ApiAcceptedResponse({ description: 'Group updated' }),
                ApiUnauthorizedResponse({ description: 'Not authorized' }),
                ApiNotFoundResponse({ description: 'Not found' }),
            ],
        },
        deleteOneBase: {
            decorators: [
                UseGuards(GroupOwnerGuard),
                UseGuards(AuthGuard('jwt')),
                ApiAcceptedResponse({ description: 'Deletes group' }),
                ApiNotFoundResponse({ description: 'Not found' }),
                ApiUnauthorizedResponse({ description: 'Not authorized' }),
            ],
        },
    },
})
@ApiTags('Groups')
@ApiBearerAuth()
@Controller('groups')
export class ContentGroupController implements CrudController<ContentGroup> {
    constructor(public readonly service: ContentGroupCrudService) { }
}