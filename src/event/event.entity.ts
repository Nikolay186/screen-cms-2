import { Screen } from "src/screen/screen.entity";
import { User } from "src/user/user.entity";
import { Column, Entity, ManyToOne, OneToMany, PrimaryGeneratedColumn } from "typeorm";


@Entity('Event')
export class Event {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column('text')
    name: string;

    @Column('text')
    owner_id: string;

    @OneToMany(() => Screen, (screen) => screen.event)
    screens: Screen[];
    
    @ManyToOne(() => User, (user) => user.events, {
        cascade: true,
        onDelete: 'CASCADE',
    })
    owner: User;
}