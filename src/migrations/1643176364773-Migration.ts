import {MigrationInterface, QueryRunner} from "typeorm";

export class Migration1643176364773 implements MigrationInterface {
    name = 'Migration1643176364773'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "Content_group" ("id" SERIAL NOT NULL, "owner_id" character varying NOT NULL, "ownerId" integer, CONSTRAINT "PK_a773e7adb1f563323071f071777" PRIMARY KEY ("id"))`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" ADD "owner_id" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Event" ADD "owner_id" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Screen" ADD "owner_id" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Playlist" ADD "owner_id" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "s3_key" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "group_id" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "owner_id" text NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "width" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "height" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "orientation" character varying NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" DROP CONSTRAINT "FK_166662d7bf89da8a342838729f2"`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" DROP CONSTRAINT "FK_f909b762287d74c643b07bd8029"`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" DROP COLUMN "playlistId"`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" ADD "playlistId" integer`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" DROP COLUMN "contentId"`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" ADD "contentId" integer`);
        await queryRunner.query(`ALTER TABLE "Content" DROP CONSTRAINT "FK_9b20dbefe2a82327f81a3388eaf"`);
        await queryRunner.query(`ALTER TABLE "Event" DROP CONSTRAINT "FK_8448e766fb3303362954f5d22a0"`);
        await queryRunner.query(`ALTER TABLE "User" DROP CONSTRAINT "PK_9862f679340fb2388436a5ab3e4"`);
        await queryRunner.query(`ALTER TABLE "User" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "User" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "User" ADD CONSTRAINT "PK_9862f679340fb2388436a5ab3e4" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Screen" DROP CONSTRAINT "FK_d9084382a02f98250b9cca0c2f6"`);
        await queryRunner.query(`ALTER TABLE "Event" DROP CONSTRAINT "PK_894abf6d0c8562b398c717414d6"`);
        await queryRunner.query(`ALTER TABLE "Event" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Event" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Event" ADD CONSTRAINT "PK_894abf6d0c8562b398c717414d6" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Event" DROP COLUMN "ownerId"`);
        await queryRunner.query(`ALTER TABLE "Event" ADD "ownerId" integer`);
        await queryRunner.query(`ALTER TABLE "Screen" DROP CONSTRAINT "PK_bf86825808b3babf300fad57b2f"`);
        await queryRunner.query(`ALTER TABLE "Screen" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Screen" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Screen" ADD CONSTRAINT "PK_bf86825808b3babf300fad57b2f" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Screen" DROP COLUMN "eventId"`);
        await queryRunner.query(`ALTER TABLE "Screen" ADD "eventId" integer`);
        await queryRunner.query(`ALTER TABLE "Playlist" DROP CONSTRAINT "PK_cbf731d0c3b4c18ee9930d14790"`);
        await queryRunner.query(`ALTER TABLE "Playlist" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Playlist" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Playlist" ADD CONSTRAINT "PK_cbf731d0c3b4c18ee9930d14790" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Content" DROP CONSTRAINT "PK_7cb78a77f6c66cb6ea6f4316a5c"`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "id" SERIAL NOT NULL`);
        await queryRunner.query(`ALTER TABLE "Content" ADD CONSTRAINT "PK_7cb78a77f6c66cb6ea6f4316a5c" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "ownerId"`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "ownerId" integer`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" ADD CONSTRAINT "FK_166662d7bf89da8a342838729f2" FOREIGN KEY ("playlistId") REFERENCES "Playlist"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" ADD CONSTRAINT "FK_f909b762287d74c643b07bd8029" FOREIGN KEY ("contentId") REFERENCES "Content"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Event" ADD CONSTRAINT "FK_8448e766fb3303362954f5d22a0" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Screen" ADD CONSTRAINT "FK_d9084382a02f98250b9cca0c2f6" FOREIGN KEY ("eventId") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Content" ADD CONSTRAINT "FK_9b20dbefe2a82327f81a3388eaf" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Content_group" ADD CONSTRAINT "FK_0887540186e187d8b2cec96dcdc" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "Content_group" DROP CONSTRAINT "FK_0887540186e187d8b2cec96dcdc"`);
        await queryRunner.query(`ALTER TABLE "Content" DROP CONSTRAINT "FK_9b20dbefe2a82327f81a3388eaf"`);
        await queryRunner.query(`ALTER TABLE "Screen" DROP CONSTRAINT "FK_d9084382a02f98250b9cca0c2f6"`);
        await queryRunner.query(`ALTER TABLE "Event" DROP CONSTRAINT "FK_8448e766fb3303362954f5d22a0"`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" DROP CONSTRAINT "FK_f909b762287d74c643b07bd8029"`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" DROP CONSTRAINT "FK_166662d7bf89da8a342838729f2"`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "ownerId"`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "ownerId" uuid`);
        await queryRunner.query(`ALTER TABLE "Content" DROP CONSTRAINT "PK_7cb78a77f6c66cb6ea6f4316a5c"`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Content" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "Content" ADD CONSTRAINT "PK_7cb78a77f6c66cb6ea6f4316a5c" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Playlist" DROP CONSTRAINT "PK_cbf731d0c3b4c18ee9930d14790"`);
        await queryRunner.query(`ALTER TABLE "Playlist" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Playlist" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "Playlist" ADD CONSTRAINT "PK_cbf731d0c3b4c18ee9930d14790" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Screen" DROP COLUMN "eventId"`);
        await queryRunner.query(`ALTER TABLE "Screen" ADD "eventId" uuid`);
        await queryRunner.query(`ALTER TABLE "Screen" DROP CONSTRAINT "PK_bf86825808b3babf300fad57b2f"`);
        await queryRunner.query(`ALTER TABLE "Screen" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Screen" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "Screen" ADD CONSTRAINT "PK_bf86825808b3babf300fad57b2f" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Event" DROP COLUMN "ownerId"`);
        await queryRunner.query(`ALTER TABLE "Event" ADD "ownerId" uuid`);
        await queryRunner.query(`ALTER TABLE "Event" DROP CONSTRAINT "PK_894abf6d0c8562b398c717414d6"`);
        await queryRunner.query(`ALTER TABLE "Event" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "Event" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "Event" ADD CONSTRAINT "PK_894abf6d0c8562b398c717414d6" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Screen" ADD CONSTRAINT "FK_d9084382a02f98250b9cca0c2f6" FOREIGN KEY ("eventId") REFERENCES "Event"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "User" DROP CONSTRAINT "PK_9862f679340fb2388436a5ab3e4"`);
        await queryRunner.query(`ALTER TABLE "User" DROP COLUMN "id"`);
        await queryRunner.query(`ALTER TABLE "User" ADD "id" uuid NOT NULL DEFAULT uuid_generate_v4()`);
        await queryRunner.query(`ALTER TABLE "User" ADD CONSTRAINT "PK_9862f679340fb2388436a5ab3e4" PRIMARY KEY ("id")`);
        await queryRunner.query(`ALTER TABLE "Event" ADD CONSTRAINT "FK_8448e766fb3303362954f5d22a0" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Content" ADD CONSTRAINT "FK_9b20dbefe2a82327f81a3388eaf" FOREIGN KEY ("ownerId") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" DROP COLUMN "contentId"`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" ADD "contentId" uuid`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" DROP COLUMN "playlistId"`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" ADD "playlistId" uuid`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" ADD CONSTRAINT "FK_f909b762287d74c643b07bd8029" FOREIGN KEY ("contentId") REFERENCES "Content"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" ADD CONSTRAINT "FK_166662d7bf89da8a342838729f2" FOREIGN KEY ("playlistId") REFERENCES "Playlist"("id") ON DELETE NO ACTION ON UPDATE NO ACTION`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "orientation"`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "height"`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "width"`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "owner_id"`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "group_id"`);
        await queryRunner.query(`ALTER TABLE "Content" DROP COLUMN "s3_key"`);
        await queryRunner.query(`ALTER TABLE "Playlist" DROP COLUMN "owner_id"`);
        await queryRunner.query(`ALTER TABLE "Screen" DROP COLUMN "owner_id"`);
        await queryRunner.query(`ALTER TABLE "Event" DROP COLUMN "owner_id"`);
        await queryRunner.query(`ALTER TABLE "Playlist_content" DROP COLUMN "owner_id"`);
        await queryRunner.query(`DROP TABLE "Content_group"`);
    }

}
