export class UserRO {
    id: string;
    username: string;
    email?: string;
    password?: string;
    token?: string;
    events?: Event[];
}