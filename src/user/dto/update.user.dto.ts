import { IsOptional } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateUserDTO {
    @ApiProperty()
    @IsOptional()
    username?: string;

    @ApiProperty()
    @IsOptional()
    email?: string;
    
    @ApiProperty()
    @IsOptional()
    newPassword?: string;
}