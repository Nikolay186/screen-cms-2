import { NotFoundException, ForbiddenException, CanActivate, ExecutionContext } from "@nestjs/common";
import { getRepository } from "typeorm";
import { Playlist } from "../playlist.entity";
import { User } from "src/user/user.entity";

export class PlaylistOwnerGuard implements CanActivate {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const playlistReposirory = getRepository(Playlist);
        const userRepository = getRepository(User);
        const playlist_id = request.params.id;

        const modPlaylist = await playlistReposirory.findOne({
            where: { id: playlist_id },
        });
        if (!modPlaylist) {
            throw new NotFoundException('Specified playlist does not exists');
        }
        const owner = await userRepository.findOne({
            where: { id: modPlaylist.owner_id }
        });

        const user = request.user;
        if (user.id == owner.id) {
            return true;
        } else {
            throw new ForbiddenException('You are not the owner of specified playlist');
        }
    }
}