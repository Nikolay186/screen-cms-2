import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Repository } from "typeorm";
import { Playlist } from "./playlist.entity";

@Injectable()
export class PlaylistService extends TypeOrmCrudService<Playlist> {
    constructor(@InjectRepository(Playlist) repository: Repository<Playlist>) {
        super(repository);
    }
}