import { IsNotEmpty } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class UpdatePlaylistDTO {
    @ApiProperty()
    @IsNotEmpty()
    name: string;
}