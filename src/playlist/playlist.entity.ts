import { PlaylistContent } from 'src/playlist_content/playlist_content.entity';
import { Screen } from 'src/screen/screen.entity';
import {
    Column,
    Entity,
    OneToMany,
    OneToOne,
    PrimaryGeneratedColumn,
} from 'typeorm';

@Entity('Playlist')
export class Playlist {
    @PrimaryGeneratedColumn('increment')
    id: string;

    @Column('text')
    name: string;

    @Column('text')
    owner_id: string;

    @OneToOne(() => Screen, (screen) => screen.playlist)
    screen: Screen;

    @OneToMany(() => PlaylistContent, (content) => content.playlist)
    playlistContent: PlaylistContent[];
}