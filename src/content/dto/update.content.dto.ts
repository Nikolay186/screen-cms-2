import { IsUrl } from "class-validator";
import { ContentKind } from "../content.entity";
import { ApiProperty } from "@nestjs/swagger";

export class UpdateContentDTO {
    @ApiProperty()
    name?: string;

    @ApiProperty()
    kind?: ContentKind;

    @ApiProperty()
    @IsUrl()
    url?: string;
}