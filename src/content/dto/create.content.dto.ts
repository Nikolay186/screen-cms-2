import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsUrl } from "class-validator";
import { ContentKind } from "../content.entity";

export class CreateContentDTO {
    @ApiProperty()
    @IsNotEmpty()
    name: string;

    @ApiProperty()
    @IsNotEmpty()
    kind: ContentKind;

    @ApiProperty()
    @IsNotEmpty()
    width: string;

    @ApiProperty()
    @IsNotEmpty()
    height: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsUrl()
    url: string;
}