import { Inject, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { AwsService } from "src/aws/aws.service";
import { Repository } from "typeorm";
import { Content, Orientation } from "./content.entity";
import { CreateContentDTO } from "./dto/create.content.dto";
import { UpdateContentDTO } from "./dto/update.content.dto";

@Injectable()
export class ContentService {
    constructor(
        @InjectRepository(Content) private readonly contentRepository: Repository<Content>,
        private readonly awsService: AwsService,
    ) { }

    async findAll(userId): Promise<Content[]> {
        return this.contentRepository.find({
            where: { owner_id: userId },
        });
    }

    async delete(id) {
        const del_content = await this.contentRepository.findOne({
            where: { id: id },
        });

        await this.contentRepository.remove(del_content);
        await this.awsService.deleteFile(del_content.s3_key);
        return del_content;
    }

    async create(content: CreateContentDTO, file) {
        const { link, key } = await this.awsService.uploadFile(file);
        let orientation = content.width > content.height ? Orientation.Landscape : Orientation.Portrait;
        this.contentRepository.save({
            ...content,
            orientation,
            url: link,
            s3_key: key,
        });
        return link;
    }

    async getContent(groupId) {
        const group = await this.contentRepository.find({
            where: { group_id: groupId },
        });

        const screen_orientation = screen.orientation.type;
        const optimal_content = group.find(cont => cont.orientation == screen_orientation);

        return { link: optimal_content.url };
    }

    async update(updateContent: UpdateContentDTO, id) {
        await this.contentRepository.update({ id }, updateContent);
        const updContent = await this.contentRepository.findOne({
            where: { id },
        });
        return updContent;
    }
}