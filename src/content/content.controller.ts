import { Body, Controller, Delete, Get, Param, Post, Put, UploadedFile, UseGuards } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { CreateContentDTO } from "./dto/create.content.dto";
import { UpdateContentDTO } from "./dto/update.content.dto";
import { ContentOwnerGuard } from "./guard/content.owner.guard";
import { ContentService } from "./content.service";
import { User } from "src/user/user.decorator";
import { AuthGuard } from "@nestjs/passport";

@ApiTags('Content')
@Controller('content')
export class ContentController {
    constructor(private readonly contentService: ContentService) { }

    @Get('/getall')
    getAll(@User('id') user) {
        return this.contentService.findAll(user);
    }

    @Get()
    getContent(@Param('id') groupId: string) {
        return this.contentService.getContent(groupId);
    }

    @Post()
    @UseGuards(AuthGuard('jwt'))
    createContent(@Body() content: CreateContentDTO, @UploadedFile() file) {
        return this.contentService.create(content, file);
    }

    @Put()
    @UseGuards(ContentOwnerGuard)
    @UseGuards(AuthGuard('jwt'))
    updateContent(@Param('id') contentId: string, @Body() data: UpdateContentDTO) {
        return this.contentService.update(data, contentId);
    }

    @Delete()
    @UseGuards(ContentOwnerGuard)
    @UseGuards(AuthGuard('jwt'))
    deleteContent(@Param('id') contentId: string) {
        return this.contentService.delete(contentId);
    }
}