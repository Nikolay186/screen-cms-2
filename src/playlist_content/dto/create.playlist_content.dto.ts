import { ApiProperty } from "@nestjs/swagger";
import { IsNumber } from "class-validator";

export class CreatePlaylistContentDTO {
    @ApiProperty()
    @IsNumber()
    position: number;

    @ApiProperty()
    @IsNumber()
    duration: number;
}