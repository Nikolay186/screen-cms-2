import { ForbiddenException, NotFoundException, CanActivate, ExecutionContext, Injectable } from "@nestjs/common";
import { getRepository } from "typeorm";
import { Playlist } from "../../playlist/playlist.entity";
import { User } from "src/user/user.entity";

@Injectable()
export class OwnerGuard implements CanActivate {
    async canActivate(context: ExecutionContext): Promise<boolean> {
        const request = context.switchToHttp().getRequest();
        const playlistReposirory = getRepository(Playlist);
        const userRepository = getRepository(User);

        const playlistId = request.params.id;
        const modPlaylist = await playlistReposirory.findOne({
            where: { id: playlistId }
        });
        if (!modPlaylist) {
            throw new NotFoundException('Specified playlist does not exists');
        }
        const owner = await userRepository.findOne({
            where: { id: modPlaylist.owner_id }
        })

        const user = request.user;
        if (user.id == owner.id) {
            return true;
        } else {
            throw new ForbiddenException('You are not the owner of specified playlist');
        }
    }
}