import { Content } from "src/content/content.entity";
import { Playlist } from "src/playlist/playlist.entity";
import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from "typeorm";

@Entity('Playlist_content')
export class PlaylistContent {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    position: number;

    @Column()
    duration: number;

    @Column('text')
    owner_id: string;

    @ManyToOne(() => Playlist, playlist => playlist.playlistContent)
    playlist: Playlist;

    @ManyToOne(() => Content, content => content.playlistContent)
    content: Content;
}