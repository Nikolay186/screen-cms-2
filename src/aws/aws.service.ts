import { Inject, Injectable, Req, Res } from "@nestjs/common";
import * as AWS from 'aws-sdk';
import { S3 } from "aws-sdk";

@Injectable()
export class AwsService {
    s3 = new AWS.S3({
        accessKeyId: process.env.AWS_S3_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_S3_KEY,
    });
    bucket_name = process.env.AWS_S3_BUCKET_NAME;

    async uploadFile(file) {
        const cfg = {
            Bucket: this.bucket_name,
            Key: `${file.originalname} - ${Date.now().toString()}`,
            Body: file.buffer,
        };

        const result = await this.s3.upload(cfg).promise();

        return { link: result.Location, key: result.Key, configuration: cfg };
    }

    async deleteFile(file_key) {
        const cfg = {
            Bucket: this.bucket_name,
            Key: file_key,
        };

        await this.s3.deleteObject(cfg).promise();
    }
}